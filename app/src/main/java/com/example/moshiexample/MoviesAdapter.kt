package com.example.moshiexample

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.moshiexample.databinding.MoviesRvItemBinding

class MoviesAdapter(val moviesList: List<Movie>) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = MoviesRvItemBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(moviesList[position])
    }

    override fun getItemCount() = moviesList.size

    inner class ViewHolder(val binding: MoviesRvItemBinding) :
            RecyclerView.ViewHolder(binding.root){

        private fun getGenresText(genre: List<Genre>) : String{
            var genreString = ""
            genre.forEach {
                genreString = "$genreString${it.name}, "
            }
            return genreString
        }

        fun setData(movie: Movie) {
            binding.titleTv.text = movie.title
            binding.overviewTv.text = movie.overview
            binding.genresTv.text = getGenresText(movie.genres)
        }
    }
}
