package com.example.moshiexample

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieAPI {

    @GET("trending/movie/week")
    fun getTrendingMoviesAsync(@Query("api_key") apiKey: String): Deferred<MoviesResponseDTO>
}