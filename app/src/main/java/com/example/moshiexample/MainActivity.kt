package com.example.moshiexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

const val API_KEY = "799923e1d5b9c3c0b45d5815166f97fc"
private var myJob: Job? = null

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        myJob = CoroutineScope(Dispatchers.IO).launch {
            // moshi supports custom adapter
            val moshi = Moshi.Builder().add(GenreAdapter()).build()
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.themoviedb.org/3/")
                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()

            val service = retrofit.create(MovieAPI::class.java)
            val trendingMoviesResponseDTO = service.getTrendingMoviesAsync(API_KEY).await()

            // withContext moves execution of the given coroutine
            withContext(Dispatchers.Main) {
                val movies = trendingMoviesResponseDTO.results
                Log.i("Movies", movies.toString())
                val moviesRecyclerView = findViewById<RecyclerView>(R.id.movies_rv)
                with(moviesRecyclerView){
                    adapter = MoviesAdapter(movies)
                    layoutManager = LinearLayoutManager(this@MainActivity)
                    addItemDecoration(
                            DividerItemDecoration(
                            context,
                            LinearLayoutManager.VERTICAL
                    )
                    )
                }

            }
        }

    }

    override fun onDestroy() {
        myJob?.cancel()
        super.onDestroy()
    }
}